#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include <QQmlContext>
#include "launcher.h"

#include <QtQml>

int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);
    QUrl src(QStringLiteral("anim.qml"));
    QQuickView view(src);
    Launcher launcher;
    QQmlContext *context = view.rootContext();

 //   qmlRegisterSingletonType("SeComponent", 1, 0, "RegisteredSingleton", app);
    // qmlRegisterSingletonType<MainWindow>("SeComponent", 1, 0, "SeColor", QML_MainWindow);
    // qmlRegisterSingletonType(QUrl("file:///home/seb/bitbucket/animation/anim/SeComponent"), 1, 0, "RegisteredSingleton", view.engine());

    context->setContextProperty("Launcher", &launcher);
    view.show();
    return app.exec();
}
