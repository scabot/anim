TEMPLATE = app
QT += qml quick
SOURCES += *.cpp
HEADERS += *.h
QML_IMPORT_PATH += SeComponent
OTHER_FILES += \
    *.qml \
    SeComponent/*.qml \
    img/*.png\
    patate/*.qml
