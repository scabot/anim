import QtQuick 2.1
import "SeComponent" 1.0
import "patate"

Rectangle {
    id: optingDialog
    width: 640
    height: 480

    color: myColor.bgColor

    property bool optIn: false

    SeColor { id: myColor } // FIXME: Make use of singleton (Not easy in Qt 5.1, but much easier in 5.2, 5.3 and 5.40

    ParallelAnimation {
        id: pop
        running: false
        NumberAnimation { target: optSel; property: "width"; to: optSel.width * 1.2; duration: 400 }
        NumberAnimation { target: optSel; property: "height"; to: optSel.height * 1.5; duration: 400 }
        NumberAnimation { target: optSel; property: "border.width"; to: optSel.border.width * 3; duration: 400 }
        NumberAnimation { target: optSel; property: "opacity"; to: 0; duration: 400 }
        onStopped: {
            optSel.init()
            console.log('restore state')
        }
    }

    function optingIn (opting) {
        if (opting === optIn) {
            pop.start()
        } else {
            optIn = opting
        }
    }

    Item {
        height: 80
        width: parent.width
        id: topBar
        Image {
            id: icon
            anchors.verticalCenter: parent.verticalCenter
            x: icon.y // Same margin space above/below and left
            source: "img/adr.png"
        }
        Text {
            text: 'Lorem ipsum dolor sit amet'
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 18
            font.bold: true
        }
    }

    Text {
        id: body
        color: "white"
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        wrapMode: Text.WordWrap
        font.pointSize: 18
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        width: parent.width
        anchors.top: topBar.bottom
        anchors.bottom: bottomHorizontalSep.top
    }

    Rectangle {
        id: bottomHorizontalSep
        height: 1
        width: parent.width
        anchors.bottom: bottomBar.top
    }

    Item {
        id: bottomBar
        width: parent.width
        height: 80

        anchors.bottom: parent.bottom

        OptingButton {
            id: optInBut
            pressed: optIn
            text: 'Opt In'
            height: parent.height
            // Set width (and x pos)
            anchors.left: parent.left
            anchors.right: verticalSep.left
            onClicked: optingIn(true)
        }
        Rectangle {
            id: verticalSep
            width: 1
            height: parent.height
            // Set x pos
            anchors.horizontalCenter: parent.horizontalCenter
        }
        OptingButton {
            pressed: !optIn
            id: optOutBut
            text: 'Opt Out'
            height: parent.height
            // Set width (and x pos)
            anchors.left: verticalSep.right
            anchors.right: parent.right
            onClicked: optingIn(false)
        }

        Rectangle {
            id: optSel

            Component.onCompleted: {
                optSel.init()
            }

            property var offset: 10
            property var but

            function init () {
                but = (optIn ? optInBut : optOutBut)

                width = but.width - (2 * offset)
                height = but.height - (2 * offset)
                anchors.horizontalCenter = but.horizontalCenter
                anchors.verticalCenter = but.verticalCenter

                border.width = 5
                opacity = 1
            }

            border.color: "white"
            radius: 20
            color: "#00000000"
}

        states: [
            State {
                StateChangeScript { script: console.log("  ==> optIn : " + optIn) }
                name: "optIn"
                when: optIn
                AnchorChanges {
                    target: optSel
                    anchors.horizontalCenter: optInBut.horizontalCenter
                }
            },
            State {
                StateChangeScript { script: console.log("  ==> optOut : " + optIn) }
                name: "optOut"
                when: !optIn
                AnchorChanges {
                    target: optSel
                    anchors.horizontalCenter: optOutBut.horizontalCenter
                }
            }
        ]
        transitions: Transition {
            // smoothly reanchor myRect and move into new position
            AnchorAnimation { duration: 450; easing.type: Easing.InOutQuart }
        }
    }
}
