import QtQuick 2.0

Rectangle {
    id: root

    property alias text: label.text
    property bool pressed: false
    signal clicked

    width: 200
    height: 80
    color: "#00000000"

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: {
            root.clicked()
        }
    }

    Text {
        id: label
        text: "Button text..."
        font.pointSize: 16
        color: "white"
        styleColor: "black"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

        anchors.fill: parent
        }

    states: [
        State {
            StateChangeScript { script: console.log("  ==> messagePopup.state = show" + label.text) }
            name: "pressed"
            when: pressed
            PropertyChanges {
                target: label
                style: Text.Raised
                font.pointSize: 20
            }
        }
    ]

    transitions: Transition {
        PropertyAnimation { properties: "font.pointSize"; easing.type: Easing.InOutQuad; duration: 450 }
    }
}
